const element = document.querySelector('#filter');
const choices = new Choices(element, {
    searchEnabled: false,
    placeholder: false,
    itemSelectText: '',
});

const swiper = new Swiper('.hero-swiper', {
    loop: true,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
        reverseDirection: true,
    },
});

const gallerySwiper = new Swiper('.gallery-swiper', {
    slidesPerView: 3,
    slidesPerColumn: 2,
    spaceBetween: 30,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + '">' + (index + 1) + '</span>';
          },
    },
});